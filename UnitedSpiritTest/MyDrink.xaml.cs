﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnitedSpiritTest
{
    /// <summary>
    /// Interaction logic for MyDrink.xaml
    /// </summary>
    public partial class MyDrink : UserControl
    {
        public MyDrink()
        {
            InitializeComponent();
        }
        public event EventHandler EventLoadMailPopup;
        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }


        private void btnClose_TouchDown_1(object sender, TouchEventArgs e)
        {
            //myDrinkSharePopUp.Visibility = Visibility.Hidden;
        }

        private void btnShareAll_TouchDown_1(object sender, TouchEventArgs e)
        {
            EventLoadMailPopup(this, null);
        }
    }
}
