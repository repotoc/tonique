﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnitedSpirits
{
    /// <summary>
    /// Interaction logic for VideoSlideshowUc.xaml
    /// </summary>
    public partial class VideoSlideshowUc : UserControl
    {
        public VideoSlideshowUc()
        {
            InitializeComponent();
        }
        public event EventHandler EventCloseVideo;
        private void MESlideshow_MediaEnded(object sender, RoutedEventArgs e)
        {
            //play video on media end
            MESlideshow.Play();
            MESlideshow.Position = new TimeSpan(0, 0, 2);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            string path = @"C:\Tonique\video.mp4";   //get the video file from specific path store in 'path' string
            if (File.Exists(path))  //check weather the video exists in the given path
            {
                MESlideshow.Source = new System.Uri(path);
                MESlideshow.Play();
                MESlideshow.Position = new TimeSpan(0, 0, 2);
            }
        }
        private void MainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            EventCloseVideo(this, null);
        }
    }
}
