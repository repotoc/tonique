﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UnitedSpirits;

namespace UnitedSpiritTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            #region Manipulation Events
            this.ManipulationStarting -= HomePage_ManipulationStarting;
            this.ManipulationDelta -= HomePage_ManipulationDelta;
            this.ManipulationStarting += HomePage_ManipulationStarting;
            this.ManipulationDelta += HomePage_ManipulationDelta;
            #endregion

            LoadHomeVideo();
        }
        #region ManipulationEvents

        ManipulationModes currentMode = ManipulationModes.All;
        private void HomePage_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }
        private void HomePage_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            if (element != null)
            {
                MatrixTransform xform = element.RenderTransform as MatrixTransform;
                Matrix matrix = xform.Matrix;
                ManipulationDelta delta = args.DeltaManipulation;
                Point center = args.ManipulationOrigin;
                matrix.Translate(-center.X, -center.Y);
                matrix.Scale(delta.Scale.X, delta.Scale.Y);
                matrix.Translate(center.X, center.Y);
                matrix.Translate(delta.Translation.X, delta.Translation.Y);
                xform.Matrix = matrix;
            }
            args.Handled = true;
            base.OnManipulationDelta(args);
        }
        #endregion
        private void LoadHomeVideo()
        {
            GridLargePanel.Children.Clear();
            VideoSlideshowUc video = new VideoSlideshowUc();
            GridLargePanel.Children.Add(video);
            video.EventCloseVideo += video_EventCloseVideo;
        }

        void video_EventCloseVideo(object sender, EventArgs e)
        {
            VideoSlideshowUc video = (VideoSlideshowUc)sender;
            GridLargePanel.Children.Remove(video);
            LoadUserAge();
        }

        private void LoadUserAge()
        {
            ua = new UserAge();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(ua);
            ua.myEventDOB += ua_myEventDOB;
        }

        UserAge ua;
        ItemDetail itemdts;
        Listing lst;
        void ua_myEventDOB(object sender, EventArgs e)
        {   
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);

            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura += lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM += lst_myeventItemDetailsWM;
        }

        void lst_myeventItemDetailsWM(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            wm = new WhyteMacan();
            ParentPanel.Children.Add(wm);
            wm.myEventWM += wm_myEventWM;
        }

        void wm_myEventWM(object sender, EventArgs e)
        {

            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);

            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura += lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM+=lst_myeventItemDetailsWM;
        }

        ItemJura itmJura;
        ItemDetDalmore itemDalmore;
        WhyteMacan wm;
        void lst_myeventItemDetailsDalmore(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            itemDalmore = new ItemDetDalmore();
            ParentPanel.Children.Add(itemDalmore);
            itemDalmore.myEventDalmoreFlip += itemDalmore_myEventDalmoreFlip;
            
        }

        void itemDalmore_myEventDalmoreFlip(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);

            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura+=lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM += lst_myeventItemDetailsWM;
        }

    
        CreateDrink cDrink;
        void lst_myeventItemDetails(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            itemdts = new ItemDetail();
            ParentPanel.Children.Add(itemdts);
            itemdts.myEventFlip += itemdts_myEventFlip;

            itemDalmore = new ItemDetDalmore();
            itemDalmore.myEventDalmoreFlip+=itemDalmore_myEventDalmoreFlip;

            itmJura = new ItemJura();
            itmJura.myEventJuraFlip += itmJura_myEventJuraFlip;

            wm = new WhyteMacan();
            wm.myEventWM+=wm_myEventWM;
        }

        void itmJura_myEventJuraFlip(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);

            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura += lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM += lst_myeventItemDetailsWM;
        }

        void lst_myeventItemDetaisJura(object sender, EventArgs e)
        {

            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            itmJura = new ItemJura();
            ParentPanel.Children.Add(itmJura);
            itmJura.myEventJuraFlip+=itmJura_myEventJuraFlip;
        }

        void itemdts_myEventFlip(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);
            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura += lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM += lst_myeventItemDetailsWM;
        }

        private void btnHome_TouchDown(object sender, TouchEventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Hidden;
            ParentPanel.Children.Clear();
            LoadHomeVideo();
        }
        UserShareLink userlnk;
        MyDrink myDrnk;
        private void btnCreateADrink_TouchDown_1(object sender, TouchEventArgs e)
        {
            ParentPanel.Children.Clear();
            cDrink = new CreateDrink();
            ParentPanel.Children.Add(cDrink);
        }

        private void btnMyDrink_TouchDown_1(object sender, TouchEventArgs e)
        {
            myDrnk = new MyDrink();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(myDrnk);
            myDrnk.EventLoadMailPopup += myDrnk_EventLoadMailPopup;
            //myDrnk.myDrinkEventHandler += myDrnk_myDrinkEventHandler;  
        }

        void myDrnk_EventLoadMailPopup(object sender, EventArgs e)
        {
            EmailUc email = new EmailUc();
            GridLargePanel.Children.Add(email);
            email.EventCloseEmailPopup += email_EventCloseEmailPopup;
            email.EventSendEmailPopup += email_EventSendEmailPopup;
        }

        void email_EventSendEmailPopup(object sender, EventArgs e)
        {
            GridLargePanel.Children.Clear();
            userlnk = new UserShareLink();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(userlnk);            
        }

        void email_EventCloseEmailPopup(object sender, EventArgs e)
        {
            EmailUc email = (EmailUc)sender;
            GridLargePanel.Children.Remove(email);
        }


        private void btnFloorPlan_TouchDown(object sender, TouchEventArgs e)
        {
            ImageViewUc image = new ImageViewUc();
            image.IsManipulationEnabled = true;
            image.RenderTransform = new MatrixTransform(1, 0, 0, 1, 10, 10);
            image.BringIntoView();
            Panel.SetZIndex(image, 999);
            GridLargePanel.Children.Add(image);
            image.EventCloseImageViewUc += image_EventCloseImageViewUc;
        }

        void image_EventCloseImageViewUc(object sender, EventArgs e)
        {
            ImageViewUc image = (ImageViewUc)sender;
            GridLargePanel.Children.Remove(image);
        }

    }
}
