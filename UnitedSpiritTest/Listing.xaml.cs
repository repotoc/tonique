﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnitedSpiritTest
{
    /// <summary>
    /// Interaction logic for Listing.xaml
    /// </summary>
    public partial class Listing : UserControl
    {
        public Listing()
        {
            InitializeComponent();
        }

        public delegate void myeventListing(object sender, EventArgs e);
        public event myeventListing myeventItemDetails;
        public event EventHandler myeventItemDetailsDalmore;
        public event EventHandler  myeventItemDetaisJura;
        public event EventHandler myeventItemDetailsWM;


        private void Button_TouchDown_1(object sender, TouchEventArgs e)
        {
            myeventItemDetails(this, null);
        }

        private void btnBlackDog_TouchDown_1(object sender, TouchEventArgs e)
        {
            myeventItemDetails(this, null);
        }

        private void btnDalmore_TouchDown_1(object sender, TouchEventArgs e)
        {
            myeventItemDetailsDalmore(this, null);
        }

        private void btnJura_TouchDown_1(object sender, TouchEventArgs e)
        {
            myeventItemDetaisJura(this, null);
        }

        private void btnWhyteMackan_TouchDown_1(object sender, TouchEventArgs e)
        {
            myeventItemDetailsWM(this, null);
        }
    }
}
