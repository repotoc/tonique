﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace UnitedSpiritTest
{
    /// <summary>
    /// Interaction logic for UserAge.xaml
    /// </summary>
    public partial class UserAge : UserControl
    {
        public UserAge()
        {
            InitializeComponent();
        }

        public event EventHandler myEventDOB;
        
        
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //UAPopup.Visibility = Visibility.Visible;
        }

        
        private void btnProceed_TouchDown_1(object sender, TouchEventArgs e)
        {

            if (CBMonth.SelectedIndex >= 0 && CBYear.SelectedIndex >= 0)
            {
                myEventDOB(this, null);
            }
        }
    }
}
