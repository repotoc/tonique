﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitedSpirits
{
    public class Apps
    {

        public static string strPath = AppDomain.CurrentDomain.BaseDirectory;

        public static void KillTabTip()
        {
            //TabTip
            try
            {
                if (Process.GetProcessesByName("TabTip").Count() > 0)
                {
                    Process fetchProcess = Process.GetProcessesByName("TabTip").FirstOrDefault();
                    if (fetchProcess != null)
                    {
                        fetchProcess.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void OpenVirtualKeyboard()
        {
            try
            {
                Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe");
            }
            catch (Exception ex)
            {
            }
        }
    }
}