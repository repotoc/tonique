﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnitedSpirits
{
    /// <summary>
    /// Interaction logic for EmailUc.xaml
    /// </summary>
    public partial class EmailUc : UserControl
    {
        public EmailUc()
        {
            InitializeComponent();
        }
        public event EventHandler EventCloseEmailPopup;
        public event EventHandler EventSendEmailPopup;
        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
            EventCloseEmailPopup(this, null);
        }

        private void txtName_TouchEnter(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
        }

        private void txtName_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void btnSendMail_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
            EventSendEmailPopup(this, null);
        }
    }
}
